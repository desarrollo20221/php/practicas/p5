<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form method="post">                    
            <label for="numero">Introduce un numero</label>
            <input type="number" name="numero" id="numero"/>
            <button>Enviar</button>
        </form>
        <?php
            $salida=''; 
            
            if($_POST){
                $numero = $_POST["numero"];
                switch ($numero){
                    case '1':
                        $salida = "Lunes";
                        break;
                    case '2':
                        $salida = "Martes";
                        break;
                    case '3':
                        $salida = "Miercoles";
                        break;
                    case '4':
                        $salida = "Jueves";
                        break;
                    case '5':
                        $salida = "Viernes";
                        break;
                    case '6':
                        $salida = "Sabado";
                        break;
                    case '7':
                        $salida = "Domingo";
                        break;
                    default :
                        $salida = "no valido";
                }
                
                echo $salida;
            }
        ?>
    </body>
</html>
